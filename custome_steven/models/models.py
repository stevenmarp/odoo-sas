from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class InheritSaleOrder(models.Model):
    _inherit = 'sale.order'
        
    req_vendor_id = fields.Many2one(
        string='Request Vendor',
        comodel_name='res.partner',
        ondelete='restrict',
    )
    
    no_kontrak = fields.Char(
        string='No Kontrak', required=True,
    )
    
    with_po = fields.Boolean(
        string='With PO',
    )

    po_ids = fields.One2many('sale.order.line', 'po_id', string='PO')

    
   
    

    # call form view PO
    def action_create_po(self):
        # new_context.update({'check_move_validity':False})
        for rec in self:
            po_value = {
                    'name': 'New',
                    'state': 'draft',
                    'partner_id': rec.partner_id.id,
                    'date_order': rec.date_order or fields.date.today(),
                    'partner_ref': rec.name or False,
                    }
            # bikin PO header
            created_po_id = self.env['purchase.order'].create(po_value)
            for line in rec.order_line:
                line_rfq_val = []
                line_rfq = {
                            'product_id': line.product_id.id,
                            'name': line.name,
                            'product_qty': line.product_uom_qty,
                            # 'product_uom' : line['uom'],
                            'order_id': created_po_id.id,
                            }
                line_rfq_val = self.env['purchase.order.line'].create(line_rfq)
                
        # return {
        #     'res_model' : 'purchase.order',
        #     'type' : 'ir.actions.act_window',
        #     'view_mode' : 'form',
        #     'view_type' : 'form',
        #     'view_id' : self.env.ref("purchase.purchase_order_form").id,
        #     'target' : 'current'
        # }
                
            form_view_ref = self.env.ref('purchase.purchase_order_form', False)
            return {
                # 'name': (_("Attendance")),
                'view_mode': 'form',
                'view_id': False,
                'view_type': 'form',
                'res_model': 'purchase.order',
                'type': 'ir.actions.act_window',
                'target': 'current',
                'res_id': created_po_id.id,
                'views': [(form_view_ref and form_view_ref.id or False, 'form')],
                'context': {}
            }




    




    # ValidationError
    @api.constrains('no_kontrak')
    def action_confirm(self):
        for rec in self:
            so = self.env['sale.order'].search([('no_kontrak', '=', rec.no_kontrak), ('id', '!=', rec.id)])
            if so:
                raise ValidationError(_("No Kontrak %s sudah pernah diinputkan sebelumnya…!" % rec.no_kontrak))
            
    




class InheritSaleOrderLines(models.Model):
    _inherit = 'sale.order.line'
    
   
    po_id = fields.Many2one(
        string='PO',
        comodel_name='sale.order',
        ondelete='restrict',
    )

    


# class CreatePopUpSo(models.TransientModel):

#     _name = 'create.pop.up.so'
#     _description = 'Create Pop Up So'



#     attachment = fields.Binary(
#         string='Attachment',
#     )