{
  'name': 'Steven Custome SAS',
  'author': 'Steven',
  'version': '14',
  'depends': [
    'sale',
  ],
  'data': [
    # 'security/ir.model.access.csv',
    'views/view.xml',
  ],
  'qweb': [
    # 'static/src/xml/nama_widget.xml',
  ],
  'sequence': 1,
  'auto_install': False,
  'installable': True,
  'application': True,
  'category': '- Arkademy Part 1',
  'summary': 'SAS Management Software',
  'license': 'LGPL-3',
  'website': 'https://stevenmarpportofolio.wenikah.com/',
  'description': '-'
}